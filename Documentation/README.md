# Use Case Documentation

This folder contains the documentation of all Use Cases and scenarios.

:warning: ***WARNING*** Be aware that although the demo environment is fully functionally and you can indeed order and deploy resources, deployment times are slower due to the technology used under the hood. Keep this in mind when demoing and do not waste time by waiting for an provisioning order to fully complete.

:heavy_check_mark: ***NOTE*** It is highly recommend to test the use cases you want to demonstrate in advance, Make yourself familiar with the environment and how things are implemented. Do not try to diverse into something you never checked yourself because you might confuse your audience.

## Lab Overview

The infrastructure of the lab is using the following technology:

- Red Hat Virtualization 4.2
- CloudForms 4.6.4 (aka CFME 5.9.4)
- Red Hat OpenStack Platform 12
- Red Hat OpenShift 3.6
- Ansible Tower 3.3.0
- VMware vSphere 5.5
- RHEL 6 IPA (RH IdM) Server, also used as NFS Storage
- RHEL 7 Workstation (used as Bastion host and SSH Gateway)

Instructions on how to access the individual systems are sent by mail when ordering the lab from RHPDS.

## Service Catalog

An example Service Catalog is populated with serveral Service Catalog Items.

[Details about the Service Catalog](service-catalog.md)

## Ansible Tower

As part of the lab environment an Ansible Tower is available. It is preconfigured with some Job Templates.

[Details about Ansible Tower](ansible-tower.md)

## Compliance Check and Enforcement

A scenario to check and enforce Virtual Machine Compliance has be implemented, by combining a Job Template configured on Ansible Tower with a Compliance and a Control Policy on CloudForms.

This use case was documented in the following blog posts:

- [Enforce SELinux Compliance Policy with Ansible](http://www.jung-christian.de/post/2017/12/enforce-selinux-with-ansible/)
- [SELinux Compliance Policy](http://www.jung-christian.de/post/2017/10/control-policy-selinux/)
- [Custom Smart State Analysis Profiles](http://www.jung-christian.de/post/2017/10/modify-ssa-profile/)

## Reporting

A few CloudForms reports can be found in the [Reports](../Reports/) sub folder.

[Details about the example Reports](../Reports/README.md)

## CMDB

CloudForms does not aim to replace a CMDB solution, however we can use custom attributes to add additional data and associate it to a given object. Custom attributes are shown in the UI if defined. They can also be utilized in Reports.

[Details about the Custom Attribute use case](custom-attributes.md)

## MIQ Expression for Dynamic Dialogs

The Automate Domain in the [../Automate}(../Automate) Sub folder has some examples for the new Expression Methods introduced with CloudForms 4.6.

You can find more details about how to create and use them in Peter McGowan's [Mastering Automation Addendum for CloudForms 4.6 and ManageIQ Gaprindashvili
](https://manageiq.gitbook.io/mastering-cloudforms-automation-addendum/chapter) book.