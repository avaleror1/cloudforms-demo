## Windows Security Profile:

#### Policies involved:
- VM and Instance Compliance: Windows Firewall Check
- VM and Instance Control: Check Compliance
- VM and Instance Control: Run SmartState

![Windows_Security_Profile_components](Documentation/Images/Windows Security Profile/Windows_Security_Profile_components.png)

“Check Compliance” and “Run SmartState” policies are already covered in “Compliance Check and Enforcement” policy, in this repo.

## Overall Policy behaviour:
Performs an SSA on VMs when “Power ON” or “Reset” events are detected , after SSA checks two conditions needed for passing the compliance policy and if both are satisfied the VM is marked as a Compliant in any other case the VM would be marked as Non-Compliant. The two conditions to be checked are two different registry keys in windows register, one for checking if firewall for public networks is running and other one for private networks.

## Windows Firewall Check:
**- Policy objective:** Try to made sure that any Windows based VM has Windows Firewall enabled.
**- How?** The easiest way to check that firewall is enabled is checking the registry using policies, since windows registry has two differents keys for Firewall Status (one for private networks and other one for public networks) both of them should be checked and made sure that values are the proper ones.

Registry keys to check:

*- HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\StandardProfile:EnableFirewall*, value should be 1

*- HKEY_LOCAL_MACHINE\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\PublicProfile:EnableFirewall*, value should be 1

Windows Firewall Check compliance policy has 2 different Conditions inside, each of this conditions will ensure that the VM OS is Windows as scope and then will check the value of the registry key.

![Windows_Firewall_Check_policy](Documentation/Images/Windows Security Profile/Windows_Firewall_Check_policy.png)

This registry keys will be checked using SSA so both registry paths should be included in SSA profile otherwise the policy won't be able to check the key´s value.

![SSA_patterns](Documentation/Images/Windows Security Profile/SSA_patterns.png)

## How to implement it?

1. Go to /Control/Explorer click in Policies and select “Vm Compliance Policies” then in the upper part of the page click on Configuration and click on Add a New VM and Instance Compliance Policy.

![new_compliance_policy](Documentation/Images/Windows Security Profile/new_compliance_policy.png)

2. In the screen opened it is necessary to fulfil the Description with a descriptive name and click save, there is no need to set an Scope since it will be done in next steps.
3. Select now the policy you just created and go to Configuration and click on “Create a new Condition assigned to this Policy”, fulfil a comprehensive Description. Now it's time to create an expression to validate, click in the drop-down menu and select Registry.

![new_condition_registry](Documentation/Images/Windows Security Profile/new_condition_-_registry.png)

4. Fulfil the registry path in Key and the field name that we’re going to check in Value (take this path from the one you created in SSA profile), in data put the value you´re looking for.
   Key=HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\PublicProfile
   Value=EnableFirewall
   Data=1

![condition_expression](Documentation/Images/Windows Security Profile/condition_expression.png)

5. Is time to set the Scope for the Condition, in this case Windows Machines only, so in drop-down inside Scope area select Field, and in next drop-down look for “OS Name” in “VM and Instance category”, select “INCLUDES” and windows as a value and click on the validate button for saving the selections done.

![condition_scope](Documentation/Images/Windows Security Profile/condition_scope.png)

6. Click Add
7. Repeat the process but for next registry values:
   Key=HKLM\SYSTEM\CurrentControlSet\Services\SharedAccess\Parameters\FirewallPolicy\StandartProfile
   Value=EnableFirewall
   Data=1
8. Click on “Policy Profiles”, select “All Policy Profiles”, click on Configuration drop-down and click again in Add a New Policy Profile.

![New_Policy_Profile](Documentation/Images/Windows Security Profile/New_Policy_Profile.png)

9. Write a Description, on Policy selection select policies you want to include in this profile one by one and click in the “>” button for including it on the profile. Select the policy you created, “Check Compliance” and “Run SmartState” policies and last but not least click Add.

![Policy_Profile](Documentation/Images/Windows Security Profile/Policy_Profile.png)

Now you if you apply to a provider, any new Windows Machine provided will get an SSA and later will be checked for Firewall Enabled in Windows.